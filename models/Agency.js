var keystone = require("keystone");
var Types = keystone.Field.Types;

/**
 * Post Model
 * ==========
 */

var Agency = new keystone.List("Agency", {
	map: { name: "title" },
	plural: "agencies",
	autokey: { path: "slug", from: "title", unique: true }
});

Agency.add({
	title: { type: String, required: true },
	state: {
		type: Types.Select,
		options: "draft, published, archived",
		default: "draft",
		index: true
	},
	author: { type: Types.Relationship, ref: "User", index: true },
	publishedDate: {
		type: Types.Date,
		index: true,
		dependsOn: { state: "published" }
	},
	image: { type: Types.CloudinaryImage },
	content: {
		link: { type: Types.Url },
		brief: { type: Types.Html, wysiwyg: true, height: 150 }
	},
	categories: { type: Types.Relationship, ref: "AgencyCategory", many: true },
	country: { type: Types.Relationship, ref: "Country", many: true },
	channel: { type: Types.Relationship, ref: "Channel", many: true }
});

Agency.schema.virtual("content.full").get(function() {
	return this.content.extended || this.content.brief;
});

Agency.defaultColumns = "title, state|20%, author|20%, publishedDate|20%";
Agency.register();
