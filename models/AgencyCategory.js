var keystone = require("keystone");

/**
 * PostCategory Model
 * ==================
 */

var AgencyCategory = new keystone.List("AgencyCategory", {
	autokey: { from: "name", path: "key", unique: true }
});

AgencyCategory.add({
	name: { type: String, required: true }
});

AgencyCategory.relationship({
	ref: "Agency",
	path: "agencies",
	refPath: "categories"
});

AgencyCategory.register();
