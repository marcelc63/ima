var keystone = require("keystone");
var Types = keystone.Field.Types;

/**
 * Enquiry Model
 * =============
 */

var Channel = new keystone.List("Channel", {
	map: { name: "title" },
	plural: "channels"
});

Channel.add({
	title: { type: String, required: true }
});

Channel.defaultColumns = "title";
Channel.register();
