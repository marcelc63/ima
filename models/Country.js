var keystone = require("keystone");
var Types = keystone.Field.Types;

/**
 * Enquiry Model
 * =============
 */

var Country = new keystone.List("Country", {
	map: { name: "title" },
	plural: "countries",
	autokey: { from: "title", path: "key", unique: true }
});

Country.add({
	title: { type: String, required: true },
	flag: { type: Types.CloudinaryImage }
});

Country.defaultColumns = "title";
Country.register();
