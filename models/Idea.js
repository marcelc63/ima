var keystone = require("keystone");
var Types = keystone.Field.Types;

/**
 * Post Model
 * ==========
 */

var Idea = new keystone.List("Idea", {
	map: { name: "title" },
	autokey: { path: "slug", from: "title", unique: true }
});

Idea.add({
	title: { type: String, required: true },
	state: {
		type: Types.Select,
		options: "draft, published, archived",
		default: "draft",
		index: true
	},
	author: { type: Types.Relationship, ref: "User", index: true },
	publishedDate: {
		type: Types.Date,
		index: true,
		dependsOn: { state: "published" }
	},
	image: { type: Types.CloudinaryImage },
	content: {
		week: { type: Types.Text, wysiwyg: true, height: 150 },
		question: { type: Types.Text, wysiwyg: true, height: 150 },
		brief: { type: Types.Text, wysiwyg: true, height: 150 },
		extended: { type: Types.Html, wysiwyg: true, height: 400 }
	}
});

Idea.schema.virtual("content.full").get(function() {
	return this.content.extended || this.content.brief;
});

Idea.relationship({ path: "comments", ref: "IdeaComment", refPath: "idea" });

Idea.defaultColumns = "title, state|20%, author|20%, publishedDate|20%";
Idea.register();
