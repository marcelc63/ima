var keystone = require("keystone");
var Types = keystone.Field.Types;

/**
	Posts
	=====
 */

var IdeaComment = new keystone.List("IdeaComment", {
	label: "Comments"
});

IdeaComment.add({
	author: { type: String },
	email: { type: String },
	post: { type: Types.Relationship, initial: true, ref: "Idea", index: true },
	commentState: {
		type: Types.Select,
		options: ["published", "draft", "archived"],
		default: "published",
		index: true
	},
	publishedOn: {
		type: Types.Date,
		default: Date.now,
		noedit: true,
		index: true
	}
});

IdeaComment.add("Content", {
	content: { type: Types.Text, wysiwyg: true, height: 300 }
});

IdeaComment.schema.pre("save", function(next) {
	this.wasNew = this.isNew;
	if (
		!this.isModified("publishedOn") &&
		this.isModified("commentState") &&
		this.commentState === "published"
	) {
		this.publishedOn = new Date();
	}
	next();
});

// PostComment.schema.post("save", function() {
// 	if (!this.wasNew) return;
// 	if (this.author) {
// 		keystone
// 			.list("User")
// 			.model.findById(this.author)
// 			.exec(function(err, user) {
// 				if (user) {
// 					user.wasActive().save();
// 				}
// 			});
// 	}
// });

IdeaComment.track = true;
IdeaComment.defaultColumns =
	"author, post, publishedOn, commentState";
IdeaComment.register();
