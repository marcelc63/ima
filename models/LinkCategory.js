var keystone = require("keystone");

/**
 * PostCategory Model
 * ==================
 */

var LinkCategory = new keystone.List("LinkCategory", {
	autokey: { from: "name", path: "key", unique: true }
});

LinkCategory.add({
	name: { type: String, required: true }
});

LinkCategory.relationship({
	ref: "Link",
	path: "links",
	refPath: "categories"
});

LinkCategory.register();
