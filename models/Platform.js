var keystone = require("keystone");
var Types = keystone.Field.Types;

/**
 * Post Model
 * ==========
 */

var Platform = new keystone.List("Platform", {
	map: { name: "title" },
	autokey: { path: "slug", from: "title", unique: true }
});

Platform.add({
	title: { type: String, required: true },
	state: {
		type: Types.Select,
		options: "draft, published, archived",
		default: "draft",
		index: true
	},
	author: { type: Types.Relationship, ref: "User", index: true },
	publishedDate: {
		type: Types.Date,
		index: true,
		dependsOn: { state: "published" }
	},
	image: { type: Types.CloudinaryImage },
	screenshots: { type: Types.CloudinaryImages },
	content: {
		link: { type: Types.Url },
		brief: { type: Types.Html, wysiwyg: true, height: 150 },
		extended: { type: Types.Html, wysiwyg: true, height: 400 }
	},
	categories: { type: Types.Relationship, ref: "PlatformCategory", many: true },
	country: { type: Types.Relationship, ref: "Country", many: true },
	channel: { type: Types.Relationship, ref: "Channel", many: true }
});

Platform.schema.virtual("content.full").get(function() {
	return this.content.extended || this.content.brief;
});

Platform.defaultColumns = "title, state|20%, author|20%, publishedDate|20%";
Platform.register();
