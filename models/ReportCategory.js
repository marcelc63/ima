var keystone = require("keystone");

/**
 * PostCategory Model
 * ==================
 */

var ReportCategory = new keystone.List("ReportCategory", {
	autokey: { from: "name", path: "key", unique: true }
});

ReportCategory.add({
	name: { type: String, required: true }
});

ReportCategory.relationship({
	ref: "Report",
	path: "reports",
	refPath: "categories"
});

ReportCategory.register();
