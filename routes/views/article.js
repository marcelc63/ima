var keystone = require("keystone");
var async = require("async");
var PostComment = keystone.list("PostComment");

exports = module.exports = function(req, res) {
	var view = new keystone.View(req, res);
	var locals = res.locals;

	// Set locals
	locals.section = "article";
	locals.filters = {
		post: req.params.post
	};
	locals.data = {
		posts: [],
		categories: []
	};
	locals.meta = {
		title: "Influencer Marketing Insights, Tips and Strategy for Asia Market",
		description:
			"Find the Latest Asia Influencer Marketing News and Resources by leading Social Media Marketing Experts, Companies, Platforms and Agencies. All in one place.",
		keywords:
			"influencer marketing, social media, influencer china, influencer south east asia, instagram, influencer marketing asia",
		canonical: req.protocol + "://" + req.get("host") + req.originalUrl
	};

	// Load all categories
	view.on("init", function(next) {
		keystone
			.list("PostCategory")
			.model.find()
			.sort("name")
			.exec(function(err, results) {
				if (err || !results.length) {
					return next(err);
				}

				locals.data.categories = results;

				// Load the counts for each category
				async.each(
					locals.data.categories,
					function(category, next) {
						keystone
							.list("Post")
							.model.count()
							.where("categories")
							.in([category.id])
							.exec(function(err, count) {
								category.postCount = count;
								next(err);
							});
					},
					function(err) {
						next(err);
					}
				);
			});
	});

	// Load the current post
	view.on("init", function(next) {
		var q = keystone
			.list("Post")
			.model.findOne({
				state: "published",
				slug: locals.filters.post
			})
			.populate("author categories country");

		q.exec(function(err, result) {
			locals.data.post = result;
			locals.meta.title = result.title;
			locals.meta.description = result.content.brief;
			next(err);
		});
	});

	// Load other posts
	view.on("init", function(next) {
		// var q = keystone
		// 	.list("Post")
		// 	.model.find({})
		// 	.where("state", "published")
		// 	.sort("-publishedDate")
		// 	.populate("author categories country")
		// 	.limit("4");
		var q = keystone
			.list("Post")
			.paginate({
				page: 1,
				perPage: 4,
				maxPages: 1,
				filters: {
					state: "published"
				}
			})
			.sort("-publishedDate")
			.populate("author categories country");

		q.exec(function(err, results) {
			locals.data.posts = results;
			next(err);
		});
	});

	// Load comments on the Post
	view.on("init", function(next) {
		PostComment.model
			.find()
			.where("post", locals.data.post)
			.where("commentState", "published")
			.sort("-publishedOn")
			.exec(function(err, comments) {
				if (err) return res.err(err);
				if (!comments) return res.notfound("Post comments not found");
				locals.data.comments = comments;
				next();
			});
	});

	// Create a Comment
	view.on("post", { action: "comment.create" }, function(next) {
		var author = req.body.name === "" ? "Guest" : req.body.name;
		var newComment = new PostComment.model({
			state: "published",
			post: locals.data.post.id,
			author: author,
			email: req.body.email,
			content: req.body.content
		});
		newComment.save(function(err) {
			// post has been saved
		});
	});

	// Render the view
	view.render("article");
};
