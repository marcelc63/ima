var keystone = require("keystone");
var async = require("async");

exports = module.exports = function(req, res) {
	var view = new keystone.View(req, res);
	var locals = res.locals;

	// Init locals
	locals.section = "articles";
	locals.filters = {
		category: req.params.category
	};
	locals.data = {
		posts: [],
		categories: []
	};
	locals.meta = {
		title: "Influencer Marketing Insights, Tips and Strategy for Asia Market",
		description:
			"Find the Latest Asia Influencer Marketing News and Resources by leading Social Media Marketing Experts, Companies, Platforms and Agencies. All in one place.",
		keywords:
			"influencer marketing, social media, influencer china, influencer south east asia, instagram, influencer marketing asia",
		canonical: req.protocol + "://" + req.get("host") + req.originalUrl
	};

	// Load all categories
	view.on("init", function(next) {
		keystone
			.list("PostCategory")
			.model.find()
			.sort("name")
			.exec(function(err, results) {
				if (err || !results.length) {
					return next(err);
				}

				locals.data.categories = results;

				// Load the counts for each category
				async.each(
					locals.data.categories,
					function(category, next) {
						keystone
							.list("Post")
							.model.count()
							.where("categories")
							.in([category.id])
							.exec(function(err, count) {
								category.postCount = count;
								next(err);
							});
					},
					function(err) {
						next(err);
					}
				);
			});
	});

	// Load the current category filter
	view.on("init", function(next) {
		if (req.params.category) {
			keystone
				.list("PostCategory")
				.model.findOne({ key: locals.filters.category })
				.exec(function(err, result) {
					locals.data.category = result;
					next(err);
				});
		} else {
			next();
		}
	});

	// Load the posts
	view.on("init", function(next) {
		var q = keystone
			.list("Post")
			.paginate({
				page: req.query.page || 1,
				perPage: 100,
				maxPages: 10,
				filters: {
					state: "published"
				}
			})
			.sort("-publishedDate")
			.populate("author categories country");

		if (locals.data.category) {
			q.where("categories").in([locals.data.category]);
		}

		q.exec(function(err, results) {
			locals.data.posts = results;
			next(err);
		});
	});

	// Render the view
	view.render("articles");
};
