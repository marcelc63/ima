var keystone = require("keystone");
var async = require("async");
var IdeaComment = keystone.list("IdeaComment");

exports = module.exports = function(req, res) {
	var view = new keystone.View(req, res);
	var locals = res.locals;

	// Set locals
	locals.section = "idea";
	locals.filters = {
		idea: req.params.idea
	};
	locals.data = {
		posts: []
	};
	locals.meta = {
		title: "Influencer Marketing Insights, Tips and Strategy for Asia Market",
		description:
			"Find the Latest Asia Influencer Marketing News and Resources by leading Social Media Marketing Experts, Companies, Platforms and Agencies. All in one place.",
		keywords:
			"influencer marketing, social media, influencer china, influencer south east asia, instagram, influencer marketing asia",
		canonical: req.protocol + "://" + req.get("host") + req.originalUrl
	};

	// Load the current post
	view.on("init", function(next) {
		var q = keystone
			.list("Idea")
			.model.findOne({
				state: "published",
				slug: locals.filters.idea
			})
			.populate("author");

		q.exec(function(err, result) {
			locals.data.post = result;
			locals.meta.title = result.title + " | Influencer Marketing";
			locals.meta.description = result.content.brief;
			next(err);
		});
	});

	// Load other posts
	view.on("init", function(next) {
		// var q = keystone
		// 	.list("Post")
		// 	.model.find({})
		// 	.where("state", "published")
		// 	.sort("-publishedDate")
		// 	.populate("author categories country")
		// 	.limit("4");
		var q = keystone
			.list("Idea")
			.paginate({
				page: 1,
				perPage: 4,
				maxPages: 1,
				filters: {
					state: "published"
				}
			})
			.sort("-publishedDate")
			.populate("author");

		q.exec(function(err, results) {
			locals.data.posts = results;
			next(err);
		});
	});

	// Load comments on the Post
	view.on("init", function(next) {
		IdeaComment.model
			.find()
			.where("post", locals.data.post)
			.where("commentState", "published")
			.sort("-publishedOn")
			.exec(function(err, comments) {
				if (err) return res.err(err);
				if (!comments) return res.notfound("Post comments not found");
				locals.data.comments = comments;
				next();
			});
	});

	// Create a Comment
	view.on("post", { action: "comment.create" }, function(next) {
		var author = req.body.name === '' ? 'Guest' : req.body.name
		var newComment = new IdeaComment.model({
			state: "published",
			post: locals.data.post.id,
			author: author,
			email: req.body.email,
			content: req.body.content
		});
		newComment.save(function(err) {
			// post has been saved
		});
	});

	// Render the view
	view.render("idea");
};
