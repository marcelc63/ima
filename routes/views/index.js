var keystone = require("keystone");

exports = module.exports = function(req, res) {
	var view = new keystone.View(req, res);
	var locals = res.locals;

	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = "home";
	locals.data = {
		posts: [],
		platforms: [],
		categories: [],
		ideas: []
	};
	locals.meta = {
		title:
			"Influencer Marketing Asia | #1 Resource to Asia Influencer Marketing Market",
		description:
			"Influencer Marketing Asia is the #1 Resource for Asia Influencer Marketing Market. Influencer Marketing Platform Reviews, Tools, Templates & Case Studies.",
		keywords:
			"influencer marketing, social media, influencer china, influencer south east asia, instagram, influencer marketing asia",
		canonical: req.protocol + "://" + req.get("host") + req.originalUrl
	};

	// Load the posts
	view.on("init", function(next) {
		var q = keystone
			.list("Post")
			.paginate({
				page: req.query.page || 1,
				perPage: 10,
				maxPages: 10,
				filters: {
					state: "published"
				}
			})
			.sort("-publishedDate")
			.populate("author categories country");

		if (locals.data.category) {
			q.where("categories").in([locals.data.category]);
		}

		q.exec(function(err, results) {
			locals.data.posts = results;
			next(err);
		});
	});

	// Load the posts
	view.on("init", function(next) {
		var q = keystone
			.list("Platform")
			.paginate({
				page: req.query.page || 1,
				perPage: 10,
				maxPages: 10,
				filters: {
					state: "published"
				}
			})
			.sort("-publishedDate")
			.populate("author categories country");

		if (locals.data.category) {
			q.where("categories").in([locals.data.category]);
		}

		q.exec(function(err, results) {
			locals.data.platforms = results;
			next(err);
		});
	});

	// Load the ideas
	view.on("init", function(next) {
		var q = keystone
			.list("Idea")
			.paginate({
				page: req.query.page || 1,
				perPage: 2,
				maxPages: 10,
				filters: {
					state: "published"
				}
			})
			.sort("-publishedDate")
			.populate("author");

		q.exec(function(err, results) {
			locals.data.ideas = results;
			next(err);
		});
	});

	// Render the view
	view.render("index");
};
