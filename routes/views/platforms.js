var keystone = require("keystone");
var async = require("async");

exports = module.exports = function(req, res) {
	var view = new keystone.View(req, res);
	var locals = res.locals;

	// Init locals
	locals.section = "articles";
	locals.filters = {
		country: req.params.country
	};
	locals.data = {
		posts: [],
		countries: [],
		country: ""
	};
	locals.meta = {
		title:
			"Influencer Marketing Asia | Influencer Marketing Asia Platforms Review",
		description:
			"Reviews of Influencer Marketing Platforms in China, Japan, South Korea, Singapore, Indonesia, Malaysia, Thailand, and more...",
		keywords:
			"influencer marketing, social media, influencer china, influencer south east asia, instagram, influencer marketing asia",
		canonical: req.protocol + "://" + req.get("host") + req.originalUrl
	};

	// Load all categories
	// view.on("init", function(next) {
	// 	keystone
	// 		.list("PlatformCategory")
	// 		.model.find()
	// 		.sort("name")
	// 		.exec(function(err, results) {
	// 			if (err || !results.length) {
	// 				return next(err);
	// 			}

	// 			locals.data.categories = results;

	// 			// Load the counts for each category
	// 			async.each(
	// 				locals.data.categories,
	// 				function(category, next) {
	// 					keystone
	// 						.list("Platform")
	// 						.model.count()
	// 						.where("categories")
	// 						.in([category.id])
	// 						.exec(function(err, count) {
	// 							category.postCount = count;
	// 							next(err);
	// 						});
	// 				},
	// 				function(err) {
	// 					next(err);
	// 				}
	// 			);
	// 		});
	// });

	// Load all categories
	view.on("init", function(next) {
		keystone
			.list("Country")
			.model.find()
			.sort("name")
			.exec(function(err, results) {
				if (err || !results.length) {
					return next(err);
				}

				locals.data.countries = results;

				// Load the counts for each category
				async.each(
					locals.data.countries,
					function(country, next) {
						keystone
							.list("Platform")
							.model.count()
							.where("country")
							.in([country.id])
							.exec(function(err, count) {
								country.postCount = count;
								next(err);
							});
					},
					function(err) {
						next(err);
					}
				);
			});
	});

	// Load the current category filter
	view.on("init", function(next) {
		if (req.params.country) {
			keystone
				.list("Country")
				.model.findOne({ key: locals.filters.country })
				.exec(function(err, result) {
					locals.data.country = result;
					next(err);
				});
		} else {
			next();
		}
	});

	// Load the posts
	view.on("init", function(next) {
		var q = keystone
			.list("Platform")
			.paginate({
				page: req.query.page || 1,
				perPage: 10,
				maxPages: 10,
				filters: {
					state: "published"
				}
			})
			.sort("-publishedDate")
			.populate("author categories country");

		if (locals.data.country) {
			q.where("country").in([locals.data.country]);
		}

		q.exec(function(err, results) {
			locals.data.posts = results;
			next(err);
		});
	});

	// Render the view
	view.render("platforms");
};
