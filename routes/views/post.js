var keystone = require("keystone");

exports = module.exports = function(req, res) {
	var view = new keystone.View(req, res);
	var locals = res.locals;

	// Set locals
	locals.section = "blog";
	locals.filters = {
		post: req.params.post
	};
	locals.data = {
		posts: []
	};
	locals.meta = {
		title:
			"Influencer Marketing Asia | The Ultimate Resource for Brands, Agencies & Influencers",
		description:
			"Influencer Marketing Asia is the #1 Resource for Influencer Marketing in Asia. Influencer Marketing Platform Reviews, Tools, Templates & Case Studies.",
		keywords:
			"influencer marketing, social media, influencer china, influencer south east asia, instagram, influencer marketing asia"
	};

	// Load the current post
	view.on("init", function(next) {
		var q = keystone
			.list("Post")
			.model.findOne({
				state: "published",
				slug: locals.filters.post
			})
			.populate("author categories");

		q.exec(function(err, result) {
			locals.data.post = result;
			next(err);
		});
	});

	// Load other posts
	view.on("init", function(next) {
		var q = keystone
			.list("Post")
			.model.find()
			.where("state", "published")
			.sort("-publishedDate")
			.populate("author")
			.limit("4");

		q.exec(function(err, results) {
			locals.data.posts = results;
			next(err);
		});
	});

	// Load comments on the Post
	view.on("init", function(next) {
		PostComment.model
			.find()
			.where("post", locals.data.post)
			.where("commentState", "published")
			.sort("-publishedOn")
			.exec(function(err, comments) {
				if (err) return res.err(err);
				if (!comments) return res.notfound("Post comments not found");
				locals.data.comments = comments;
				next();
			});
	});

	// Render the view
	view.render("post");
};
