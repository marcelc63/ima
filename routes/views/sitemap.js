var keystone = require("keystone");
var async = require("async");
var sm = require("sitemap");

exports = module.exports = function(req, res) {
	// Load the posts
	var q = keystone
		.list("Post")
		.paginate({
			page: req.query.page || 1,
			perPage: 100,
			maxPages: 10,
			filters: {
				state: "published"
			}
		})
		.sort("-publishedDate")
		.populate("author categories country");

	q.exec(function(err, results) {
		var pages = [
			{
				url: "/",
				lastmodISO: new Date().toISOString(),
				changefreq: "daily",
				priority: 1
			},
			{
				url: "/articles",
				lastmodISO: new Date().toISOString(),
				changefreq: "daily",
				priority: 0.8
			},
			{
				url: "/platforms",
				lastmodISO: new Date().toISOString(),
				changefreq: "daily",
				priority: 0.8
			}
		];
		var posts = results.results.map(x => {
			return {
				url: "/article/" + x.slug,
				lastmodISO: new Date().toISOString(),
				changefreq: "daily",
				priority: 0.7
			};
		});

		var sitemap = sm.createSitemap({
			hostname: "https://influencermarketingasia.com",
			cacheTime: 600000, // 600 sec cache period
			urls: pages.concat(posts)
		});
		res.header("Content-Type", "application/xml");
		res.send(sitemap.toString());
	});
};
