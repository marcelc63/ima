function youtubeCalcSlider() {
	var e = document.getElementById("monthRange");
	(p = document.getElementById("monthRes")),
		(monthBlock = document.getElementById("resPerMonth")),
		(yearBlock = document.getElementById("resPerYear")),
		(check = document.getElementById("monthRange").value),
		(resPerYear = 365 * check),
		(perMonth = 30 * check),
		(p.innerHTML = numberWithCommas(e.value)),
		(monthBlock.innerHTML = numberWithCommas(perMonth)),
		(yearBlock.innerHTML = numberWithCommas(resPerYear));
	var n = document.getElementById("view").value;
	if (
		((minDaily = document.getElementById("minDaily")),
		(maxDaily = document.getElementById("maxDaily")),
		(minMonthly = document.getElementById("minMonthly")),
		(maxMonthly = document.getElementById("maxMonthly")),
		(minYarly = document.getElementById("minYarly")),
		(maxYarly = document.getElementById("maxYarly")),
		n <= 20)
	)
		var a = 0.35 + n / 1e3;
	else
		21 <= n && n <= 40
			? (a = 0.45 + n / 1e3)
			: 41 <= n && n <= 50
				? (a = 1.85 + n / 1e3)
				: 51 <= n && n <= 60
					? (a = 2.35 + n / 1e3)
					: 61 <= n && n <= 70
						? (a = 3.15 + n / 1e3)
						: 71 <= n && n <= 80
							? (a = 3.85 + n / 1e3)
							: 81 <= n && n <= 90
								? (a = 4.5 + n / 1e3)
								: 91 <= n && n <= 100 && (a = 5.5 + n / 1e3);
	(ValPercent = 0.25 * a),
		(minValDaily = ((a - ValPercent) * check) / 1e3),
		(maxValDaily = ((a + ValPercent) * check) / 1e3),
		(minValMonthly = Math.round(30 * minValDaily)),
		(maxValMonthly = Math.round(30 * maxValDaily)),
		(minValYarly = Math.round(365 * minValDaily)),
		(maxValYarly = Math.round(365 * maxValDaily)),
		(minDaily.innerHTML = "$" + numberWithCommas(minValDaily.toFixed(2))),
		(maxDaily.innerHTML = "$" + numberWithCommas(maxValDaily.toFixed(2))),
		(minMonthly.innerHTML = "$" + numberWithCommas(minValMonthly)),
		(maxMonthly.innerHTML = "$" + numberWithCommas(maxValMonthly)),
		(minYarly.innerHTML = "$" + numberWithCommas(minValYarly)),
		(maxYarly.innerHTML = "$" + numberWithCommas(maxValYarly));
}

function numberWithCommas(e) {
	return e.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
jQuery(document).ready(function() {
	jQuery("#monthRange1").ionRangeSlider({
		type: "single",
		grid: !0,
		from: 2e4,
		min: 0,
		max: 5e4,
		grid: !0,
		grid_num: 5,
		onChange: function(e) {
			jQuery("#monthRange").val(e.from),
				jQuery(".noxinf").fadeIn(),
				youtubeCalcSlider();
		}
	}),
		jQuery("#view1").ionRangeSlider({
			type: "single",
			grid: !0,
			from: 50,
			min: 0,
			max: 100,
			postfix: " %",
			grid: !0,
			grid_num: 4,
			onChange: function(e) {
				jQuery("#view").val(e.from),
					jQuery(".noxinf").fadeIn(),
					youtubeCalcSlider();
			},
			onFinish: function(e) {
				ga("send", "event", "youtube calculator interaction");
			}
		});
});
